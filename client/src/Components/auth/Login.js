import React, {Fragment ,useState} from 'react';
import { Link } from 'react-router-dom'; 

const Register = () => {
  const [formdata, setformdata] = useState({
    email:'',
    password:'',
  });
  const {email, password}=formdata;

  const onChange = e=>
  setformdata({...formdata, [e.target.name]: e.target.value});

  const onSubmit = async e =>{
    e.preventDedault();
  
          console.log('success');
        

        
  };


    return (
        <Fragment>
     <h1 className="large text-primary">Sign In</h1>
      <p className="lead"><i className="fas fa-user"></i> Sign In Your Account</p>
      <form className="form" onSubmit={e =>onSubmit(e)} >
        <div className="form-group">
          <input type="email" placeholder="Email Address" name="email" value={email} onChange={e=>onChange(e)}required /> 
        </div>
        <div className="form-group">
          <input type="password" placeholder="Password" name="password" value={password} onChange={e=>onChange(e)} minLength="6"/>
        </div>
        <input type="submit" className="btn btn-primary" value="Login" />
      </form>
      <p className="my-1">
        Don't have an account? <Link to="/register">Register</Link>
      </p>
        </Fragment>   
    );
};

export default Register;