const express = require('express');
const router = express.Router();



const checkAuth = require('../../middleware/auth');


/************************************
 * ADMIN CONTROLLERS
 ***********************************/
const AdminloginControllers= require('../Controllers/Users.Controllers');

/**************************************************************
 * @ROUTE       - /admin/signup
 * @METHOD      - POST
***************************************************************/
router.post('/signup', AdminloginControllers.usersignuppost);


/**************************************************************
 * @ROUTE       - /admin/login
 * @METHOD      - POST
***************************************************************/
router.post("/login", checkAuth,AdminloginControllers.userloginpost);


/***************************************************************
 * Admin middleware 
 ***************************************************************/
module.exports = router;
