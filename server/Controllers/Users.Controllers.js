
const bcrypt = require('bcryptjs');
const _ = require('lodash');
const {User,validate} = require('../Models/User.Models');



// router.get('/me', auth, async (req, res) => {
//   const user = await User.findById(req.user._id).select('-password');
//   res.send(user);
// });

  /****************************
 * @DESC - ADMIN SIGNUP
 *****************************/

exports.usersignuppost=('/', async (req, res) => {
  const { error } = validate(req.body); 
  if (error) return res.status(400).send(error.details[0].message);

  let user = await User.findOne({ email: req.body.email });
  if (user) return res.status(400).send('User already registered.');

  user = new User(_.pick(req.body, ['name', 'email','mobile','dob','password']));
  const salt = await bcrypt.genSalt(10);
  user.password = await bcrypt.hash(user.password, salt);
  await user.save();

  const token = user.generateAuthToken();
  res.header('x-auth-token', token).send(_.pick(user, ['_id', 'name', 'email','mobile','dob']));
});


  /****************************
 * @DESC - ADMIN LOGIN
 *****************************/
    exports.userloginpost=(req, res, next) => {
      User.find({email: req.body.email})
      .exec()
      .then( User => {
         if (User.length<1){
           return res.status(404).json({
           massage:'mail not found,user doees\'t exit'
          });
        }
        bcrypt.compare(req.body.password,User[0].password,(err,result) =>{
        if(err){
          return res.status(401).json({
            massage:'login failed'
          });
        }
        if(result){
           return res.status(200).json({
             massage:'login sucessful'
           });
        }
        res.status(401).json({
          massage:'password mismatch'
         });
        });
       })
      .catch(err =>{
        console.log(err);
        res.status(500).json({
          err
        });
      });
       
      }
