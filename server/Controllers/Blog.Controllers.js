

const mongoose = require('mongoose');


const BLOGPOST = require('../Models/Blog.Models');


/****************************
 * @DESC - CREATE A Blog
 ****************************/

exports.BlogControllersPOST =(req, res, next) => {
    const product = new BLOGPOST  ({
      _id: new mongoose.Types.ObjectId(),
      Title: req.body.Title,
      Name: req.body.Name,
      Description: req.body.Description,
      Body: req.body.Body,
      Author: req.body.Author
    });
    product
      .save()
      .then(result => {
        console.log(result);
        res.status(201).json({
          message: "Created Blog successfully",
          createdBlog: {
              Title: result.Title,
              Name:result.Name,
              Description:result.Description,
              Body:result.Body,
              Author:result.Author,
              Date:result.Date,
              _id: result._id,
              request: {
                  type: 'GET',
                  url: "http://localhost:3000/admin/blog/delete" + result._id
              }
          }
        });
      })
      .catch(err => {
        console.log(err);
        res.status(500).json({
          error: err
        });
      });
  }

  /****************************
 * @DESC - read/fetch A Blog
 ****************************/
exports.BlogControllersGET=(req, res, next) => {
  BLOGPOST.find()
      .select("Title Name Author Date _id")
      .exec()
      .then(docs => {
        const response = {
          count: docs.length,
          BLOGPOST: docs.map(doc => {
            return {
              Title: doc.Title,
              Name: doc.Name,
              Date: doc.Date,
              Author:doc.Author,
              _id: doc._id,
              request: {
                type: "GET",
                url: "http://localhost:3000/admin/blog/update/" + doc._id
              }
            };
          })
        };
        //   if (docs.length >= 0) {
        res.status(200).json(response);
        //   } else {
        //       res.status(404).json({
        //           message: 'No entries found'
        //       });
        //   }
      })
      .catch(err => {
        console.log(err);
        res.status(500).json({
          error: err
        });
      });
  }

   /****************************
 * @DESC - Update A Blog
 ****************************/

exports.BlogControllersPATCH=(req, res, next) => {
  const id = req.params.BlogId;
  const updateOps = {};
  for (const ops of req.body) {
    updateOps[ops.propTitle] = ops.value;
    // updateOps[ops.oldName] = ops.newName;
    // updateOps[ops.oldTitle] = ops.newTitle;
    // updateOps[ops.oldDescription] = ops.newDescription;
    // updateOps[ops.oldBody] = ops.newBody;
    // updateOps[ops.oldAuthor] = ops.newAuthor;
  }
  BLOGPOST.update({ _id: id }, { $set: updateOps })
    .exec()
    .then(result => {
      res.status(200).json({
          message: 'Blog updated',
          request: {
              type: 'GET',
              url: 'http://localhost:3000/admin/blog/get-Blog' + id
          }
      });
    })
    .catch(err => {
      console.log(err);
      res.status(500).json({
        error: err
      });
    });
}
   /****************************
 * @DESC - Delete A Blog
 ****************************/

exports.BlogControllersDELETE=(req, res, next) => {
  const id = req.params.BlogId;
  BLOGPOST.remove({ _id: id })
    .exec()
    .then(result => {
      res.status(200).json({
          message: 'Blog deleted',
          request: {
              type: 'POST',
              url: 'http://localhost:3000/',
              body:{
                  Title: 'String', 
                  Name: 'String',
                  Description:'String',
                  Body:'String',
                  Author:'String'
                  }
          }
      });
    })
    .catch(err => {
      console.log(err);
      res.status(500).json({
        error: err
      });
    });
}