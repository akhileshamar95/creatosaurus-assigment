
const config = require('config');
const jwt = require('jsonwebtoken');
const Joi = require('joi');
const mongoose = require('mongoose');

const productSchema = mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,
    Title:{type: String, required: true},
    Name: { type: String, required: true},
    Description:{ type: String , required: true },
    Body: { type: String, required: true },
    Author: { type: String, required: true},
    Date: { type: Date, default: Date.now },

    // image:{ type: Number},
});

module.exports = mongoose.model('BLOGPOST', productSchema);